#!/bin/bash

# Source the network configuration file
source network_config.sh

# Define wireless interface and connection names
interface="wlan0"
client_connection_name="ClientConnection"
hotspot_connection_name="HotspotConnection"

if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root."
    exit 1
fi

echo "Interface name: $interface"

if [[ "$1" == "--client" ]]; then
    # Check if already in client mode
    if nmcli connection show --active | grep -q "$client_connection_name"; then
        echo "Already in client mode."
        exit 0
    fi

    echo "Configuring the WiFi interface as a client..."

    # Remove existing hotspot connection if present
    nmcli connection delete "$hotspot_connection_name" >/dev/null 2>&1

    # Remove existing client connection if present
    nmcli connection delete "$client_connection_name" >/dev/null 2>&1

    # Add client connection with a name
    nmcli connection add type wifi ifname "$interface" \
                                    con-name "$client_connection_name" \
                                    autoconnect yes \
                                    ssid "$client_ssid" \
                                    mode infrastructure \
                                    wifi-sec.key-mgmt wpa-psk \
                                    wifi-sec.psk "$client_psk"

    # Connect as client
    nmcli connection up "$client_connection_name"

    echo "Client configuration completed."

elif [[ "$1" == "--hotspot" ]]; then
    # Check if already in hotspot mode
    if nmcli connection show --active | grep -q "$hotspot_connection_name"; then
        echo "Already in hotspot mode."
        exit 0
    fi

    echo "Configuring the WiFi interface as a hotspot..."

    # Remove existing hotspot connection if present
    nmcli connection delete "$hotspot_connection_name" >/dev/null 2>&1

    # Remove existing client connection if present
    nmcli connection delete "$client_connection_name" >/dev/null 2>&1

    # Create hotspot connection
    nmcli connection add type wifi ifname "$interface" \
                                    con-name "$hotspot_connection_name" \
                                    autoconnect yes \
                                    ssid "$hotspot_ssid" \
                                    mode ap \
                                    wifi-sec.key-mgmt wpa-psk \
                                    wifi-sec.psk "$hotspot_psk"

    # Manually assign a range of IP addresses for clients
    nmcli connection modify "$hotspot_connection_name" ipv4.method shared
    nmcli connection modify "$hotspot_connection_name" ipv4.addresses "$hotspot_gateway/24"

    # Activate the hotspot
    nmcli connection up "$hotspot_connection_name"

    echo "Hotspot configuration completed."

else
    echo "Usage: sudo $0 --client    Configure the WiFi interface as a client"
    echo "       sudo $0 --hotspot   Configure the WiFi interface as a hotspot"
    exit 1
fi
