#!/bin/bash

source /home/antx/ANT-X/raspberrypi-tools/variables.sh

#echo "Launch unifi server container"
#if ! screen -list | grep -q "unifi_server"; then
#    screen -L -Logfile /tmp/unifi_server.log -dmS unifi_server bash -c 'cd /home/antx/ANT-X/unifi-controller && docker compose up'
#    echo "unifi server started"
#else
#    echo "unifi server already running"
#fi

#sleep 5s

echo "Launch bridge container"
if ! screen -list | grep -q "bridge"; then
    screen -L -Logfile /tmp/bridge.log -dmS bridge $DOCKER_PX4_PATH/bridge/run_bridge.sh
    echo "bridge started"
else
    echo "bridge already running"
fi

sleep 5s

echo "Launch guidance container"
if ! screen -list | grep -q "guidance"; then
    screen -L -Logfile /tmp/guidance.log -dmS guidance $GUIDANCE_SRC_ROOT/run_guidance_container.sh
    echo "guidance started"
else
    echo "guidance already running"
fi

sleep 5s

/home/antx/ANT-X/raspberrypi-tools/start_capture.sh
