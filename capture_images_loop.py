from picamera2 import Picamera2
import time
import os

# Inizializza la fotocamera
picam2 = Picamera2()

# Configura la risoluzione della fotocamera
camera_config = picam2.create_still_configuration(main={"size": (4000, 3000)})  # Imposta la risoluzione a 1920x1080
picam2.configure(camera_config)

picam2.start()

# Crea una cartella con data e ora corrente
folder_name = time.strftime("%Y%m%d_%H%M%S")
os.makedirs(f"/home/antx/Pictures/Camera/{folder_name}")

# Inizializza il contatore
counter = 1

try:
    while True:
        # Scatta una foto e salva con timestamp e numero sequenziale
        timestamp = time.strftime("%Y%m%d_%H%M%S")
        file_name = f"photo_{counter}_{timestamp}.jpg"
        picam2.capture_file(f"/home/antx/Pictures/Camera/{folder_name}/{file_name}")
        counter += 1
        time.sleep(1)  # Attendi 1 secondo
except KeyboardInterrupt:
    # Ferma la fotocamera quando si interrompe lo script
    picam2.stop()
    print("Script interrotto.")
