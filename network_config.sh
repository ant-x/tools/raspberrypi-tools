# Parameters for the client connection
client_ssid="your_wifi_ssid"
client_psk="your_wifi_password"

# Parameters for the hotspot connection
hotspot_ssid="raspberrypi_ap"
hotspot_psk="flaiarto"
#hotspot_band="bg"  # 2.4 GHz
hotspot_band="a"    # 5 GHz
hotspot_channel="149"   # 2.4 GHz: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13
                        # 5 GHz: 36, 40, 44, 48, 149, 153, 157, 161, 165
                        # see "iw list", "iw dev", "iw reg get"
hotspot_gateway="192.168.8.1"
